package me.richardoruna.test.designuitable;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import me.richardoruna.test.ui.detalleFragment;
import me.richardoruna.test.ui.listaFragment;


public class DetalleActivity extends Activity implements detalleFragment.OnDetalleFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        String elemento="";
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            elemento = extras.getString("elemento");
        }

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        detalleFragment detalle_fragment = new detalleFragment().newInstance(elemento,"");
        fragmentTransaction.add(R.id.detalle, detalle_fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detalle, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDetalleFragmentInteraction(Uri uri) {
    }

}

package me.richardoruna.test.designuitable;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import me.richardoruna.test.ui.detalleFragment;
import me.richardoruna.test.ui.listaFragment;


public class MainActivity extends ActionBarActivity implements listaFragment.OnFragmentInteractionListener,
        detalleFragment.OnDetalleFragmentInteractionListener {

    private listaFragment lista_fragment;
    private detalleFragment detalle_fragment;
    private boolean visible_detalle=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.narajando));

        if (findViewById(R.id.detalle) != null) {
            visible_detalle = true;
        }else{
            visible_detalle = false;
        }

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        lista_fragment = new listaFragment().newInstance(visible_detalle);
        fragmentTransaction.add(R.id.list, lista_fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(String elemento) {
        Log.i("INFO","Elemento "+elemento);

        if(visible_detalle){
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if(detalle_fragment==null){
                detalle_fragment = new detalleFragment().newInstance(elemento,"");
                fragmentTransaction.add(R.id.detalle, detalle_fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commitAllowingStateLoss();//sirve para debug, ya que si el commit, falla te dice porq fallo
            }else{
                detalle_fragment = new detalleFragment().newInstance(elemento,"");
                fragmentTransaction.replace(R.id.detalle, detalle_fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commitAllowingStateLoss();
            }
        }else{
            Intent i = new Intent(MainActivity.this,DetalleActivity.class);
            i.putExtra("elemento",elemento);
            startActivity(i);
        }

    }

    @Override
    public void onDetalleFragmentInteraction(Uri uri) {

    }
}
